// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MFDMGameMode.generated.h"

UCLASS(minimalapi)
class AMFDMGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMFDMGameMode();
};



