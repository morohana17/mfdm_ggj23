// Copyright Epic Games, Inc. All Rights Reserved.

#include "MFDMPlayerController.h"
#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "MFDMCharacter.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"

#include "Public/GameTileBase.h"

AMFDMPlayerController::AMFDMPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	CachedDestination = FVector::ZeroVector;
	FollowTime = 0.f;
	MoveToTimeLimit = 1.f;
	bEnableMoveTo = true;
}

void AMFDMPlayerController::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Add Input Mapping Context
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}
}

void AMFDMPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(InputComponent))
	{
		// Setup mouse input events
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Started, this, &AMFDMPlayerController::OnInputStarted);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Triggered, this, &AMFDMPlayerController::OnSetDestinationTriggered);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Completed, this, &AMFDMPlayerController::OnSetDestinationReleased);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Canceled, this, &AMFDMPlayerController::OnSetDestinationReleased);

		// Setup touch input events
		// EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Started, this, &AMFDMPlayerController::OnInputStarted);
		// EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Triggered, this, &AMFDMPlayerController::OnTouchTriggered);
		// EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Completed, this, &AMFDMPlayerController::OnTouchReleased);
		// EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Canceled, this, &AMFDMPlayerController::OnTouchReleased);
	}
}

void AMFDMPlayerController::OnInputStarted()
{
	StopMovement();
}

// Triggered every frame when the input is held down
void AMFDMPlayerController::OnSetDestinationTriggered()
{
	if (!bEnableMoveTo)
		return;
	// We flag that the input is being pressed
	FollowTime += GetWorld()->GetDeltaSeconds();
	
	// We look for the location in the world where the player has pressed the input
	FHitResult Hit;
	bool bHitSuccessful = false;
	if (bIsTouch)
	{
		bHitSuccessful = GetHitResultUnderFinger(ETouchIndex::Touch1, ECollisionChannel::ECC_Visibility, true, Hit);
	}
	else
	{
		bHitSuccessful = GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, Hit);
	}

	// If we hit a surface, cache the location
	if (bHitSuccessful)
	{
		AGameTileBase* Tile = Cast<AGameTileBase>(Hit.GetActor());
		bValidDestination = false;
		if (Tile) {
			if (Tile->GetCharacterOnTopOfTile() == nullptr) {
				CachedDestination = Tile->GetActorLocation();
				bValidDestination = true;
			}
		}
	}
}

void AMFDMPlayerController::OnSetDestinationReleased()
{
	if (!bEnableMoveTo)
		return;
	// If we found a Tile
	if (bValidDestination)
	{
		// We move there and spawn some particles
		UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, CachedDestination);
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, FXCursor, CachedDestination, FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
	
		FTimerDelegate TimeoutDelegate = FTimerDelegate::CreateUObject(this, &AMFDMPlayerController::MoveToTimeout);
		FTimerHandle TimeoutHandle;
		UWorld* World = GetWorld();
		if (World) {
			FTimerManager &timeMan = World->GetTimerManager();
			timeMan.SetTimer(TimeoutHandle, TimeoutDelegate, MoveToTimeLimit, false);
		}
	}

	OnMoveToInputFinished.Broadcast();
}

// Triggered every frame when the input is held down
void AMFDMPlayerController::OnTouchTriggered()
{
	bIsTouch = true;
	OnSetDestinationTriggered();
}

void AMFDMPlayerController::OnTouchReleased()
{
	bIsTouch = false;
	OnSetDestinationReleased();
}

void AMFDMPlayerController::SetMoveToInputEnabled(bool bEnable)
{
	bEnableMoveTo = bEnable;
}

void AMFDMPlayerController::MoveToTimeout_Implementation()
{
	StopMovement();
	GetPawn()->SetActorLocation(CachedDestination);
}

