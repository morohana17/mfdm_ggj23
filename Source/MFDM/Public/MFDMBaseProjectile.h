// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MFDMBaseProjectile.generated.h"

class UAttributeComp;
class AMFDMCharacter;

UCLASS()
class MFDM_API AMFDMBaseProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMFDMBaseProjectile();

	UPROPERTY(BlueprintReadWrite, meta = (ExposeOnSpawn="true"))
	UAttributeComp* OwnerAttr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxSpeed = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Lifespan = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool DestroyOnHit = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void HitTarget(AMFDMCharacter* TargetChar);

};
