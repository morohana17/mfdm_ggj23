// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AttributeComp.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FHealthChanged, float, CurrHealth);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MFDM_API UAttributeComp : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UAttributeComp();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
	float HP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
	float maxHP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
	FString name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
	float defense;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
	float damage;

	UFUNCTION(BlueprintCallable, Category = "Attributes") 
	void Attack(UAttributeComp* target);

	UFUNCTION(BlueprintCallable, Category = "Attributes") 
	void Heal(float healAmount);

	UPROPERTY(BlueprintAssignable)
	FHealthChanged OnHealthChanged;

	// Start turn modifiers
	// Simulate battle between two characters
	
};
