// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MFDTurnManager.generated.h"

class AMFDMCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FRoundFinished);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEncounterFinished);

UCLASS()
class MFDM_API AMFDTurnManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMFDTurnManager();

	UFUNCTION(BlueprintCallable, Category = "TurnManager")
	void InitParticipants(TArray<AMFDMCharacter*> Participants);

	UFUNCTION(BlueprintNativeEvent, Category = "TurnManager")
	void RollInitiative();

	UFUNCTION(BlueprintCallable, Category = "TurnManager")
	void StartEncounter();

	UFUNCTION(BlueprintCallable, Category = "TurnManager")
	void EndEncounter();

	UFUNCTION(BlueprintCallable, Category = "TurnManager")
	void SetRoundPaused(bool bIsPaused);

	UFUNCTION(BlueprintImplementableEvent)
	void OnNextTurnBP();

	UFUNCTION(BlueprintImplementableEvent)
	void OnRoundFinishedBP();

	UFUNCTION(BlueprintImplementableEvent)
	void OnEncounterFinishedBP();

	UFUNCTION(BlueprintImplementableEvent)
	void OnParticipantDiedBP(const FString& Name);

	UPROPERTY(BlueprintAssignable)
	FRoundFinished OnRoundFinished;

	UPROPERTY(BlueprintAssignable)
	FEncounterFinished OnEncounterFinished;

	UFUNCTION(BlueprintCallable, Category = "TurnManager")
	TArray<FString> GetParticipantList();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void DoNextTurn();

	UFUNCTION()
	void OnParticipantDied(AMFDMCharacter* Char);

	UFUNCTION()
	bool CheckIsEncounterOver();

	UPROPERTY(BlueprintReadWrite)
	TArray<AMFDMCharacter*> TurnParticipants;
private:
	int TurnIndex;
	bool bPaused;
};
