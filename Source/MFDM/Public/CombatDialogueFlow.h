// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Engine/DataTable.h" 
#include "CombatDialogueFlow.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class MFDM_API UCombatDialogueFlow : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UDataTable* StartCombatDialogue;

	UFUNCTION(BlueprintCallable)
	bool CheckDialogueForTurnNumber(int TurnNum, UDataTable*& DialogueTable);

	UFUNCTION(BlueprintCallable)
	bool CheckDialogueForEnemyCount(int EnemyCount, UDataTable*& DialogueTable);

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<int, UDataTable*> TurnCountDialogueMap;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<int, UDataTable*> EnemyCountDialogueMap;

};
