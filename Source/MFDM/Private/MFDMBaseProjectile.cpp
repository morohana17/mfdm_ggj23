// Fill out your copyright notice in the Description page of Project Settings.


#include "MFDMBaseProjectile.h"
#include "AttributeComp.h"
#include "MFDM/MFDMCharacter.h"

// Sets default values
AMFDMBaseProjectile::AMFDMBaseProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetLifeSpan(Lifespan);

}

// Called when the game starts or when spawned
void AMFDMBaseProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

void AMFDMBaseProjectile::HitTarget_Implementation(AMFDMCharacter* TargetChar)
{
	if (!TargetChar)
		return;

	OwnerAttr->Attack(TargetChar->AttributeSet);

	if (DestroyOnHit)
		Destroy();
}

