// Fill out your copyright notice in the Description page of Project Settings.


#include "AttributeComp.h"

// Sets default values for this component's properties
UAttributeComp::UAttributeComp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UAttributeComp::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UAttributeComp::Attack(UAttributeComp *target)
{
	float totalDmg = damage - target->defense;
	if (totalDmg < 0)
		return;
	target->HP -= totalDmg;
	target->OnHealthChanged.Broadcast(target->HP);
}

void UAttributeComp::Heal(float healAmount)
{
	HP += healAmount;
	if (HP > maxHP)
	{
		HP = maxHP;
		OnHealthChanged.Broadcast(HP);
	}
}

