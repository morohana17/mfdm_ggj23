// Fill out your copyright notice in the Description page of Project Settings.


#include "MFDTurnManager.h"
#include "MFDM/MFDMCharacter.h"

// Sets default values
AMFDTurnManager::AMFDTurnManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	bPaused = true;
}

// Called when the game starts or when spawned
void AMFDTurnManager::BeginPlay()
{
	Super::BeginPlay();
}

void AMFDTurnManager::InitParticipants(TArray<AMFDMCharacter*> Participants)
{
	TurnParticipants.Empty();
	TurnParticipants.Append(Participants);
	TurnIndex = 0;
	RollInitiative();

	for (auto &&TurnChar : TurnParticipants)
	{
		TurnChar->OnTurnFinished.RemoveAll(this);
		TurnChar->OnTurnFinished.AddDynamic(this, &AMFDTurnManager::DoNextTurn);
		TurnChar->OnCharacterDeath.AddDynamic(this, &AMFDTurnManager::OnParticipantDied);
	}
	
}

void AMFDTurnManager::RollInitiative_Implementation()
{
	// Randomize turns
	for (int i = TurnParticipants.Num() - 1; i >= 0; --i)
	{
		int j = FMath::Rand() % (i + 1);
		if (i != j) TurnParticipants.Swap(i, j);
	}
}

void AMFDTurnManager::OnParticipantDied(AMFDMCharacter* Char)
{
	int i = TurnParticipants.Find(Char);
	if (i < TurnIndex)
		TurnIndex--;
	TurnParticipants.Remove(Char);
	OnParticipantDiedBP(Char->AttributeSet->name);
}

void AMFDTurnManager::StartEncounter()
{
	TurnIndex = 0;
	bPaused = false;

	for (auto &&TurnChar : TurnParticipants)
	{
		TurnChar->EnterInitiative();
	}
	DoNextTurn();
}

void AMFDTurnManager::EndEncounter()
{
	OnEncounterFinishedBP();
	OnEncounterFinished.Broadcast();
			
	for (auto &&TurnChar : TurnParticipants)
	{
		TurnChar->ExitEncounter();
	}
}

bool AMFDTurnManager::CheckIsEncounterOver()
{
	int AliveCount = 0;
	for (auto &&Char : TurnParticipants)
	{
		if (Char->IsAlive() && (!Char->AttributeSet->name.Contains("DM")))
			AliveCount++;
	}
	return AliveCount <= 1;
}

void AMFDTurnManager::DoNextTurn()
{
	if (bPaused)
		return;
	if (TurnIndex >= TurnParticipants.Num()) {
		OnRoundFinishedBP();
		OnRoundFinished.Broadcast();
		if (CheckIsEncounterOver()) {
			EndEncounter();
			bPaused = true;
		}
		TurnIndex = 0;
	}

	OnNextTurnBP();
	AMFDMCharacter* TurnChar = TurnParticipants[TurnIndex];
	TurnIndex++;
	TurnChar->RecieveTurn();
}

void AMFDTurnManager::SetRoundPaused(bool bIsPaused)
{
	bPaused = bIsPaused;
	if (bIsPaused == false)
		DoNextTurn();
}

TArray<FString> AMFDTurnManager::GetParticipantList()
{
	TArray<FString> Names;

	for (auto &&Char : TurnParticipants)
	{
		Names.Add(Char->AttributeSet->name);
	}
	return Names;
}
