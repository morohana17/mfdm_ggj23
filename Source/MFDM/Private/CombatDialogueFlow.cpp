// Fill out your copyright notice in the Description page of Project Settings.


#include "CombatDialogueFlow.h"

bool UCombatDialogueFlow::CheckDialogueForEnemyCount(int EnemyCount, UDataTable*& DialogueTable)
{
    if (EnemyCountDialogueMap.Contains(EnemyCount)) {
        DialogueTable = EnemyCountDialogueMap[EnemyCount];
        return true;
    }
    return false;
}

bool UCombatDialogueFlow::CheckDialogueForTurnNumber(int TurnNum, UDataTable*& DialogueTable)
{
    if (TurnCountDialogueMap.Contains(TurnNum)) {
        DialogueTable = TurnCountDialogueMap[TurnNum];
        return true;
    }
    return false;
}
