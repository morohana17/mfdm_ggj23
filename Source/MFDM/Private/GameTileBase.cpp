// Fill out your copyright notice in the Description page of Project Settings.


#include "GameTileBase.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "UObject/ConstructorHelpers.h"

#include "MFDM/MFDMCharacter.h"

// Sets default values
AGameTileBase::AGameTileBase()
{
	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	BoxCollision->SetBoxExtent(FVector(50.f, 50.f, 10.f));
	SetRootComponent(BoxCollision);

	TileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TileMesh"));
	TileMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	TileMesh->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> StaticMesh(TEXT("/Game/Meshes/Tiles/SM_TileBase"));
	if (StaticMesh.Object != nullptr)
	{
		TileMesh->SetStaticMesh(StaticMesh.Object);
	}

}

// Called when the game starts or when spawned
void AGameTileBase::BeginPlay()
{
	Super::BeginPlay();
	
}

AMFDMCharacter* AGameTileBase::GetCharacterOnTopOfTile()
{
	TArray<AActor *> Actors;

	BoxCollision->GetOverlappingActors(Actors, AMFDMCharacter::StaticClass());

	if (Actors.IsValidIndex(0))
		return Cast<AMFDMCharacter>(Actors[0]);
	return nullptr;
}
