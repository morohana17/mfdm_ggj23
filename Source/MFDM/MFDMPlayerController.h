// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Templates/SubclassOf.h"
#include "GameFramework/PlayerController.h"
#include "InputActionValue.h"
#include "MFDMPlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FMoveToInputFinished);

/** Forward declaration to improve compiling times */
class UNiagaraSystem;

UCLASS()
class AMFDMPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AMFDMPlayerController();

	/** Time Threshold to know if it was a short press */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	float ShortPressThreshold;

	/** FX Class that we will spawn when clicking */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UNiagaraSystem* FXCursor;

	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	class UInputMappingContext* DefaultMappingContext;
	
	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	class UInputAction* SetDestinationClickAction;

	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	class UInputAction* SetDestinationTouchAction;

	UFUNCTION(BlueprintCallable, Category=Input)
	void SetMoveToInputEnabled(bool bEnable);

	UPROPERTY(BlueprintAssignable)
	FMoveToInputFinished OnMoveToInputFinished;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MoveToTimeLimit;


protected:
	UFUNCTION(BlueprintNativeEvent)
	void MoveToTimeout();

	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	virtual void SetupInputComponent() override;
	
	// To add mapping context
	virtual void BeginPlay();

	/** Input handlers for SetDestination action. */
	void OnInputStarted();
	void OnSetDestinationTriggered();
	void OnSetDestinationReleased();
	void OnTouchTriggered();
	void OnTouchReleased();

	UPROPERTY(BlueprintReadOnly)
	bool bValidDestination;
	UPROPERTY(BlueprintReadOnly)
	FVector CachedDestination;
private:

	bool bEnableMoveTo; // Are we accepting move to inputs
	bool bIsTouch; // Is it a touch device
	float FollowTime; // For how long it has been pressed
};


