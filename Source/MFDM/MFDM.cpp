// Copyright Epic Games, Inc. All Rights Reserved.

#include "MFDM.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MFDM, "MFDM" );

DEFINE_LOG_CATEGORY(LogMFDM)
 