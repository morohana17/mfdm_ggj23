// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "Public/AttributeComp.h"
#include "MFDMCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCharacterDeath, AMFDMCharacter*, Char);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTurnFinished);

UCLASS(Blueprintable)
class AMFDMCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AMFDMCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintNativeEvent)
	void StartDeath();

	UFUNCTION(BlueprintCallable)
	void FinishDeath();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsAlive();

	UPROPERTY(BlueprintAssignable)
	FCharacterDeath OnCharacterDeath;

	// Turns
	UFUNCTION(BlueprintNativeEvent, Category = "TurnTaker")
	void EnterInitiative();

	UFUNCTION(BlueprintNativeEvent, Category = "TurnTaker")
	void ExitEncounter();

	UFUNCTION(BlueprintNativeEvent, Category = "TurnTaker")
	void RecieveTurn();

	UPROPERTY(BlueprintCallable, BlueprintAssignable, Category = "TurnTaker")
	FTurnFinished OnTurnFinished;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Attribute, meta = (AllowPrivateAccess = "true"))
	UAttributeComp* AttributeSet;
protected:
	UFUNCTION(BlueprintCallable, Category = "TurnTaker")
	void EndTurn();

	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
	void UpdateHealthbar(float health);

	UFUNCTION()
	void CheckDeath(float health);

private:
};

