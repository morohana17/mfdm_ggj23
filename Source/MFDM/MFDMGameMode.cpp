// Copyright Epic Games, Inc. All Rights Reserved.

#include "MFDMGameMode.h"
#include "MFDMPlayerController.h"
#include "MFDMCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMFDMGameMode::AMFDMGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AMFDMPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/BP_PlayerCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}