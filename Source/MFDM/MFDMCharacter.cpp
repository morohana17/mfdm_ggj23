// Copyright Epic Games, Inc. All Rights Reserved.

#include "MFDMCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Public/MFDMCharHealthbar.h"

AMFDMCharacter::AMFDMCharacter()
{
	AttributeSet = CreateDefaultSubobject<UAttributeComp>(TEXT("AttributeComp"));
	AttributeSet->HP = 100;
	AttributeSet->maxHP = 100;
	AttributeSet->defense = 10;
	AttributeSet->damage = 20;
	AttributeSet->OnHealthChanged.AddDynamic(this, &AMFDMCharacter::UpdateHealthbar);
	AttributeSet->OnHealthChanged.AddDynamic(this, &AMFDMCharacter::CheckDeath);
}

void AMFDMCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
}

void AMFDMCharacter::CheckDeath(float Health)
{
	if (Health <= 0.f)
		StartDeath();
}

void AMFDMCharacter::StartDeath_Implementation()
{
	// Play some animations
}

void AMFDMCharacter::FinishDeath()
{
	OnCharacterDeath.Broadcast(this);
	Destroy();
}

bool AMFDMCharacter::IsAlive()
{
	return (AttributeSet->HP > 0);
}

void AMFDMCharacter::EnterInitiative_Implementation()
{
}

void AMFDMCharacter::ExitEncounter_Implementation()
{
}

void AMFDMCharacter::RecieveTurn_Implementation()
{
}

void AMFDMCharacter::EndTurn()
{
	OnTurnFinished.Broadcast();
}